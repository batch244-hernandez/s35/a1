const express = require("express");

const mongoose = require("mongoose");

const app = express();
const port = 3001;

mongoose.connect("mongodb+srv://admin:admin@zuitt-course-booking.2rebnhm.mongodb.net/b244-to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", ()=>{console.log("We're connected to the database")});

// Create a User schema.
const userSchema = new mongoose.Schema({
	username : String,
	password: String
});

// Create a User model.
const User = mongoose.model("User", userSchema);

app.use(express.json());

app.use(express.urlencoded({extended: true}));

// Create a POST route that will access the "/signup" route that will create a user.

app.post("/signup", (req, res) => {
	User.findOne({username : req.body.username}, (err, result) => {
		if (req.body.username == null || req.body.password == null) {
			return res.send("Please input BOTH username and password");
		}
		if (req.body.username == "" || req.body.password == "") {
			return res.send("Username or password must not be blank");
		} 
		if (result != null && result.username == req.body.username) {
			return res.send("Username already exists");
		} else {
			let newUser = new User({
			 	username : req.body.username,
			 	password : req.body.password				
			});
			newUser.save((saveErr, savedUser) => {
				if(saveErr){
					return console.error(saveErr);
				} else {
					return res.status(201).send("New user registered");
				}
			})
		}
	})
});


app.listen(port, () => {console.log(`Server running at port ${port}`)});